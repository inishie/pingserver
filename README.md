
**pingServer**

	指定端末へのPingポーリング

---

[TOC]

---

## Usage

	ポーリングの設定ファイルをvbsの引数に指定して起動します。
	期待する状態でない場合、所定のコマンドを実行します。
	ex) cscript //nologo pingServer.vbs  pingServerConfig.ini

	ポーリングの設定は iniファイル で指定できます。
	ex) pingServerConfig.ini
		ipAddress : 状態確認先のIPアドレス ex: "127.0.0.1"
		isSuccess : 確認モード(True:稼働前提, False:停止前提)
		triggerBatch : 相違発生時のバッチファイル(引数にIPが指定されます) ex: "sendmail.bat"


## Note

	・vbs, bat, ini は日本語を含むため、ファイルのエンコードはsjisを前提としています。
	・｢稼働の確認｣｢停止の確認｣｢確認端末別のコマンド｣を想定し、パラメータにファイルを指定する方式にしています。
	・確認先が ping(ICMP) に応答する状態である必要があります。


---

## References

	WMI Fun !!
	WMIにて使用する各種オブジェクトを定義・生成する。
	http://www.wmifun.net/sample/win32_pingstatus.html

	Microsoft
	Win32\_PingStatus class | Microsoft Docs
	https://docs.microsoft.com/en-us/previous-versions/windows/desktop/wmipicmp/win32-pingstatus

	Microsoft
	ExecuteGlobal ステートメント | Microsoft Docs
	https://msdn.microsoft.com/ja-jp/library/cc392448.aspx


## Author

	inishie
	inishie77@hotmail.com
	https://bitbucket.org/inishie/

## License

Copyright (c) 2022 inishie.  
Released under the [MIT license](https://en.wikipedia.org/wiki/MIT_License).
