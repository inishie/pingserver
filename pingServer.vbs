' Pingポーリング
' ex) cscript //nologo pingServer.vbs  pingServerConfig.ini

Option Explicit

Dim ipAddress, isSuccess, triggerBatch  ' iniパラメータ
Dim isDiff, triggerCmd
Dim objShell, objArgs, objFileSys

Set objArgs = WScript.Arguments
Set objFileSys = CreateObject("Scripting.FileSystemObject")

If objArgs.Count <> 1 Then
	'引数は必須
	WScript.Echo "設定ファイルを指定してください"
	WScript.Quit 1
End If
If (False = objFileSys.FileExists(objArgs(0))) Then
	'引数は必須
	WScript.Echo "設定ファイルを指定してください"
	WScript.Quit 1
End If

' 設定値読込
ExecuteGlobal objFileSys.OpenTextFile(objArgs(0)).ReadAll()

If (ipAddress = "" Or triggerBatch = "") Then
	'環境設定は必須
	WScript.Echo "環境設定してください"
	WScript.Quit 1
End If


Set objShell = CreateObject("WScript.Shell")

isDiff = (isSuccess <> evaluationPing(ipAddress))
triggerCmd = "cmd /C " & triggerBatch & "  " & ipAddress

If isDiff Then
	' 一致しない
	objShell.Run triggerCmd, 0, True
End If

'使用した各種オブジェクトを後片付けする。
Set objShell = Nothing
Set objFileSys = Nothing
WScript.Quit 0

'--------------------------------------
'PING実行  (応答の有無を返却)
'  ipAddress  IPアドレス
Function evaluationPing(ipAddress)

	Dim oClassSet
	Dim oClass
	Dim oLocator
	Dim oService

	'ローカルコンピュータに接続する。
	Set oLocator = WScript.CreateObject("WbemScripting.SWbemLocator")
	Set oService = oLocator.ConnectServer
	'クエリー条件を WQL にて指定する。
	Set oClassSet = oService.ExecQuery("Select * From Win32_PingStatus Where Address = '" & ipAddress & "'")

	evaluationPing = False

	'コレクションを解析する。
	For Each oClass In oClassSet

		Select Case oClass.StatusCode
			Case 0
				'Success
				evaluationPing = True
			Case 11010
				'Request Timed Out
			Case Else
				'その他のエラー
		End Select
	Next

	Set oClassSet = Nothing
	Set oService = Nothing
	Set oLocator = Nothing
	Set oClass = Nothing

End Function